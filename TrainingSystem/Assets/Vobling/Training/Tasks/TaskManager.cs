using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vobling.Training.Events;
using EventType = Vobling.Training.Events.EventType;

namespace Vobling.Training.Tasks
{
    public class TaskManager : MonoBehaviour, IPublisher
    {
        public List<Task> Tasks;

        public IEventHub EventHub { get ; set; }

        void Update()
        {
            foreach (Task t in Tasks)
            {
                if (t.TaskState != TaskState.Pending)
                    continue;

                TaskState taskState = t.Update();

                if (taskState == TaskState.Success)
                    PublishTaskEvent(t, EventType.TaskCompleted);

                else if (taskState == TaskState.Failure)
                    PublishTaskEvent(t, EventType.TaskFailed);
            }
        } 

        public void HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            foreach (Task t in Tasks)
            {
                if (t.TaskState != TaskState.Pending)
                    continue;

                TaskState taskState = t.HandleTrackedEvent(publishedEvent);

                if (taskState == TaskState.Success)
                    PublishTaskEvent(t, EventType.TaskCompleted);

                else if (taskState == TaskState.Failure)
                    PublishTaskEvent(t, EventType.TaskFailed);
            }
        }

        private void PublishTaskEvent(Task task, EventType eventType)
        {
            var taskEvent = new TrackedEvent(Time.time.ToMilliseconds(), eventType, new EventParams() { TrackedObject = task.GetComponent<TrackedObject>() });
            Publish(taskEvent);
        }

        public void Publish(TrackedEvent trackedEvent)
        {
            EventHub.Publish(trackedEvent);
        }
    }
}