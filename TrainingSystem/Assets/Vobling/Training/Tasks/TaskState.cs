﻿namespace Vobling.Training.Tasks
{
    public enum TaskState
    {
        Pending,
        Success,
        Failure,
        CriticalFailure
    }
}