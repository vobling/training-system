﻿using Vobling.Training.Events;

namespace Vobling.Training.Tasks
{
    public interface ITask
    {
        TaskState TaskState { get; }
        TaskState Update();
        TaskState HandleTrackedEvent(TrackedEvent publishedEvent);
    }
}