using System;
using System.Collections.Generic;
using UnityEngine;
using Vobling.Training.Events;

namespace Vobling.Training.Tasks
{
    public abstract class Condition : MonoBehaviour, ICondition
    {
        [TextArea(3,15)]
        public string Description;

        public bool IsCleared { get; protected set; }

        public abstract bool CheckState();
        public abstract bool HandleTrackedEvent(TrackedEvent publishedEvent);
    }
}
