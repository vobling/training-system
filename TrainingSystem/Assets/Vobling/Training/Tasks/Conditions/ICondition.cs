﻿using Vobling.Training.Events;

namespace Vobling.Training.Tasks
{
    public interface ICondition
    {
        bool CheckState();
        bool HandleTrackedEvent(TrackedEvent publishedEvent);
    }
}