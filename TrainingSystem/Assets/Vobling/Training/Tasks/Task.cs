﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Vobling.Training.Events;

namespace Vobling.Training.Tasks
{
    [RequireComponent(typeof(TrackedObject))]
    public class Task : MonoBehaviour, ITask
    {
        public bool Active { get; private set; } = true;

        public string Name;

        [TextArea(5, 20)]
        public string Description;

        public TaskState TaskState { get; private set; }

        /// <summary>
        /// Conditions that determine Task completion.
        /// </summary>
        public List<Condition> ClearConditions;

        /// <summary>
        /// Conditions that determine Task failure.
        /// </summary>
        public List<Condition> FailConditions;

        /// <summary>
        /// Conditions that trigger a critical failure.
        /// </summary>
        public List<Condition> CriticalFailureConditions;

        public TaskState Update()
        {
            if (!Active)
                return TaskState;

            foreach (Condition c in CriticalFailureConditions)
            {
                if (c.CheckState())
                {
                    TaskState = TaskState.CriticalFailure;
                    return TaskState;
                }
            }

            foreach (Condition c in FailConditions)
            {
                if (c.CheckState())
                {
                    TaskState = TaskState.Failure;
                    return TaskState;
                }
            }

            bool taskSuccess = true;
            foreach (Condition c in ClearConditions)
            {
                if (!c.CheckState())
                    taskSuccess = false;
            }

            TaskState = taskSuccess ? TaskState.Success : TaskState.Pending;
            return TaskState;
        }

        public TaskState HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            if (!Active)
                return TaskState;

            foreach (Condition c in CriticalFailureConditions)
            {
                if (c.HandleTrackedEvent(publishedEvent))
                {
                    TaskState = TaskState.CriticalFailure;
                    return TaskState;
                }
            }

            foreach (Condition c in FailConditions)
            {
                if (c.HandleTrackedEvent(publishedEvent))
                {
                    TaskState = TaskState.Failure;
                    return TaskState;
                }
            }

            bool allCleared = true;
            foreach (Condition c in ClearConditions)
            {
                if (!c.HandleTrackedEvent(publishedEvent))
                    allCleared = false;
            }

            TaskState = allCleared ? TaskState.Success : TaskState.Pending;
            return TaskState;
        }
    }
}