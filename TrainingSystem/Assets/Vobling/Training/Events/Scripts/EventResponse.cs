﻿using System;
using UnityEngine.Events;

namespace Vobling.Training.Events
{
    /// <summary>
    /// Serializable wrapper for UnityEvent<TrackedEvent> (The Inspector does not support generics.)
    /// </summary>
    [Serializable]
    public class EventResponse : UnityEvent<TrackedEvent> { }
}