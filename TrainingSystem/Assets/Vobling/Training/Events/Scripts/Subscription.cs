using System;
using UnityEngine;

namespace Vobling.Training.Events
{ 
    [Serializable]
    public class Subscription
    {
        [SerializeField]
        private EventType eventType;
        public EventType EventType
        {
            get => eventType;
            set => eventType = value;
        }

        [SerializeReference]
        private IEventHub eventHub;
        public IEventHub EventHub
        {
            get => eventHub;
            private set => eventHub = value;
        }

        [SerializeReference]
        private ISubscriber subscriber;
        public ISubscriber Subscriber
        {
            get => subscriber;
            private set => subscriber = value;
        }

        public EventResponse EventResponse
        {
            get => subscriber.EventResponse;
        }

        public Subscription(ISubscriber subscriber, IEventHub eventHub, EventType eventType)
        {
            this.subscriber = subscriber;
            this.eventHub = eventHub;
            this.eventType = eventType;
        }
    }
}
