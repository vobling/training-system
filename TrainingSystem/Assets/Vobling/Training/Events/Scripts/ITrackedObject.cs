namespace Vobling.Training.Events
{
    public interface ITrackedObject
    {
       string Id { get; }
    }
}
