namespace Vobling.Training.Events
{
    public interface IEventResolver
    {
        bool ResolveEvent(TrackedEvent trackedEvent);
    }
}
