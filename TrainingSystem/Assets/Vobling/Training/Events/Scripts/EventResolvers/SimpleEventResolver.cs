﻿using System.Collections.Generic;

namespace Vobling.Training.Events
{
    /// <summary>
    /// Resolver that returns true on equal event types regardless of event parameters.
    /// </summary>
    public class SimpleEventResolver : IEventResolver
    {
        public IList<EventType> EventTypes { get; set; }

        public SimpleEventResolver(IList<EventType> eventTypes)
        {
            EventTypes = eventTypes;
        }

        public bool ResolveEvent(TrackedEvent trackedEvent)
        {
            return EventTypes.Contains(trackedEvent.EventType);
        }
    }
}
