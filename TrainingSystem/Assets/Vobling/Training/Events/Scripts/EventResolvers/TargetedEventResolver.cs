using System.Collections.Generic;

namespace Vobling.Training.Events
{ 
    /// <summary>
    /// Resolver that returns true on equal event types regardless of event parameters.
    /// </summary>
    public class TargetedEventResolver /*: IEventResolver*/
    {
        public Dictionary<EventType, List<TrackedObject>> TargetObjectsByEventType;

        //public TargetedEventResolver(IList<TargetedEvent> targetedEvents)
        //{
        //    TargetObjectsByEventType = new Dictionary<EventType, List<TrackedObject>>();

        //    foreach(var te in targetedEvents)
        //    {
        //        if (!TargetObjectsByEventType.ContainsKey(te.EventType))
        //        {
        //            TargetObjectsByEventType.Add(te.EventType, new List<TrackedObject>());
        //        }

        //        TargetObjectsByEventType[te.EventType].Add(te.TargetObject);
        //    }
        //}

        //public bool ResolveEvent(TrackedEvent trackedEvent)
        //{
        //    if (!TargetObjectsByEventType.ContainsKey(trackedEvent.EventType))
        //        return false;

        //    // TODO: optimize.
        //    return TargetObjectsByEventType[trackedEvent.EventType].Contains(trackedEvent.TrackedObject);
        //}
    }
}
