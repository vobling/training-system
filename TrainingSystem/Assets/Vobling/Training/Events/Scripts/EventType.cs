namespace Vobling.Training.Events
{
    // TODO: Create interface for event types.
    public enum EventType
    {
        ButtonPressed,
        ObjectInteraction,
        TaskCompleted,
        TaskFailed,
        ProcedureCompleted,
        ProcedureFailed
    }
}