using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    /// <summary>
    /// Abstract MonoBehaviour implementation of ISubscriber.
    /// </summary>
    public abstract class EventSubscriber : MonoBehaviour, ISubscriber
    {
        [SerializeField]
        public EventResponse eventResponse;
        public EventResponse EventResponse => eventResponse;

        public IEventHub EventHub { get; set; } 
        public abstract IList<EventType> EventTypes { get; }
        public abstract void HandleTrackedEvent(TrackedEvent publishedEvent);

        protected virtual void Start()
        {
            foreach (EventType et in EventTypes)
            {
                EventHub.Subscribe(this, et);
            }
        }

        protected virtual void OnDestroy()
        {
            foreach (EventType et in EventTypes)
            {
                EventHub.Unsubscribe(this, et);
            }
        }
    }
}
