using System;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    /// <summary>
    /// EventSubscriber that invokes it's EventResponse when an event subscribed to is published.
    /// </summary>
    [Serializable]
    public class SimpleEventSubscriber : EventSubscriber
    {
        public enum MatchType
        {
            None,
            AtLeastOne,
            All
        }

        public MatchType ShouldMatchParameters = MatchType.None;
        private List<EventType> eventTypes;
        public override IList<EventType> EventTypes => eventTypes;

        public List<TrackedEvent> SubscribedEvents;

        private void Awake()
        {
            eventTypes = new List<EventType>();
            foreach (var trackedEvent in SubscribedEvents)
            {
                eventTypes.Add(trackedEvent.EventType);
            }
        }

        public override void HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            if (ShouldMatchParameters == MatchType.None)
            {
                if (eventTypes.Contains(publishedEvent.EventType))
                    EventResponse?.Invoke(publishedEvent);
            }
            else
            {
                foreach (TrackedEvent se in SubscribedEvents)
                {
                    if (CheckEventParameters(se, publishedEvent, ShouldMatchParameters))
                    {
                        EventResponse?.Invoke(publishedEvent);
                        break;
                    }
                }
            }
        }

        // Checks for parameter equality.
        private bool CheckEventParameters(TrackedEvent a, TrackedEvent b, MatchType matchingType)
        {
            if (matchingType == MatchType.None)
                return true;


            int matchCount = 0;
            foreach(ParamType pt in a.EventParams.ParamTypes)
            {
                switch (pt)
                {
                    case ParamType.Int:
                        if (a.EventParams.Int == b.EventParams.Int)
                            matchCount++;

                        else if (matchingType == MatchType.All)
                            return false;

                        break;

                    case ParamType.String:
                        if (a.EventParams.String == b.EventParams.String)
                            matchCount++;

                        else if (matchingType == MatchType.All)
                            return false;

                        break;

                    case ParamType.TrackedObject:
                        if (a.EventParams.TrackedObject == b.EventParams.TrackedObject)
                            matchCount++;

                        else if (matchingType == MatchType.All)
                            return false;

                        break;
                }

                if (matchingType == MatchType.AtLeastOne && matchCount > 0)
                    return true;
            }

            if (matchingType == MatchType.All && matchCount == a.EventParams.ParamTypes.Count)
                return true;

            return false;
        }
    }
}
