using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    /// <summary>
    /// EventSubscriber that subscribes to a list of TrackedEvents and invokes it's EventResponse when the TrackedEvents in its List are published in order.
    /// </summary>
    public class OrderedEventSubscriber : EventSubscriber
    {
        public override IList<EventType> EventTypes => throw new System.NotImplementedException();

        public override void HandleTrackedEvent(TrackedEvent trackedEvent)
        {
            throw new System.NotImplementedException();
        }
    }
}
