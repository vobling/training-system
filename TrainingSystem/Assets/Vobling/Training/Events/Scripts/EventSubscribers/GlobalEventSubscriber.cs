using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    /// <summary>
    /// EventSubscriber that subscribes to all TrackedEvent types and directly invokes 
    /// it's EventTracker.Response when a TrackedEvent of any type is published.
    /// </summary>
    public class GlobalEventSubscriber : EventSubscriber
    {
        [SerializeField]
        private List<EventType> exceptions;
        private IList<EventType> eventTypes;
        public override IList<EventType> EventTypes => eventTypes.Except(exceptions).ToList();

        private void Awake()
        {
            eventTypes = Enum.GetValues(typeof(EventType)) as EventType[];                
        }

        public override void HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            EventResponse?.Invoke(publishedEvent);
        }
    }
}
 