using System.Collections.Generic;

namespace Vobling.Training.Events
{
    public interface ISubscriber
    {
        /// <summary>
        /// EventHub from which TrackedEvents are subscribed to.
        /// </summary>
        IEventHub EventHub { get; set; }

        /// <summary>
        /// Response invoked when a subscribed event is published through the referenced EventHub.
        /// </summary>
        EventResponse EventResponse { get; }

        /// <summary>
        /// Handle an event published through the EventHub. Do more specific filtering, etc.
        /// </summary>
        void HandleTrackedEvent(TrackedEvent publishedEvent);
    }
}
