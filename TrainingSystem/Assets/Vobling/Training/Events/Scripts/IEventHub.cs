using System.Collections.Generic;

namespace Vobling.Training.Events
{
    /// <summary>
    /// Event hub interface. Provides methods to subscribe to and publish TrackedEvents.
    /// </summary>
    public interface IEventHub
    {
        void Publish(TrackedEvent trackedEvent);
        void Subscribe(ISubscriber subscriber, EventType eventType);
        void Subscribe(ISubscriber subscriber, IEnumerable<EventType> eventTypes);
        void Unsubscribe(ISubscriber subscriber, EventType eventType);
        void Unsubscribe(ISubscriber subscriber, IEnumerable<EventType> eventTypes);
    }
}
