using System;

namespace Vobling.Training.Events
{
    public interface IPublisher
    {
        /// <summary>
        /// EventHub through which events will be published.
        /// </summary>
        IEventHub EventHub { get; set; }

        /// <summary>
        /// Publish the given TrackedEvent through the IEventHub.
        /// </summary>
        /// <param name="trackedEvent"></param>
        /// Consider replacing with "event Action<TrackedEvent> EventPublished";
        void Publish(TrackedEvent trackedEvent);
    }
}
