﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    public class EventPublisher : MonoBehaviour, IPublisher
    {
        public List<TrackedEvent> EventsToPublish;

        public IEventHub EventHub { get; set; }

        /// <summary>
        /// Publish all TrackedEvents in EventsToPublish.
        /// </summary>
        /// Note: Called from Inspector.
        public void Publish()
        {
            foreach (TrackedEvent te in EventsToPublish)
            {
                Publish(te);
            }
        }

        /// <summary>
        /// Publish the given TrackedEvent through the referenced IEventHub.
        /// </summary>
        /// <param name="trackedEvent"></param>
        public void Publish(TrackedEvent trackedEvent)
        {
            EventHub.Publish(trackedEvent);
        }
    }
}