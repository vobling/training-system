using System;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    [Serializable]
    public class TrackedObject : MonoBehaviour, ITrackedObject
    {
        // TODO: Make readonly from inspector.
        [SerializeField]
        private string id;
        public string Id { get { return id; } }

        private void Reset()
        {
            // Generate GUID.
            this.id = Guid.NewGuid().ToString();
        }
    }
}
