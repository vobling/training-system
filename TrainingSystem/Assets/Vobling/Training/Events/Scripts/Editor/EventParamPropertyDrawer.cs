﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Vobling.Training.Events
{
    [CustomPropertyDrawer(typeof(EventParams))]
    public class EventParamPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            EditorGUI.LabelField(new Rect(position.x, position.y, 120, 20), "Event Param");
            EditorGUI.indentLevel = indentLevel;

            Rect typeRect = new Rect(position.x, position.y + 22, 450, 20);
            Rect paramRect = new Rect(position.x, position.y + 44, 450, 20);

            var paramList = property.FindPropertyRelative("ParamTypes");
            int mask = GetEnumMask(paramList);

            var paramNames = Enum.GetNames(typeof(ParamType));
            mask = EditorGUI.MaskField(typeRect, "Param Types", mask, paramNames);

            paramList.ClearArray();
            for (int i = 0; i < paramNames.Length; i++)
            {
                int type = 1 << i;
                if ((mask & type) != 0)
                {
                    // selected layer.
                    ParamType pt = (ParamType)Enum.Parse(typeof(ParamType), paramNames[i]);

                    int current = paramList.arraySize;
                    paramList.InsertArrayElementAtIndex(current);
                    var prop = paramList.GetArrayElementAtIndex(current);
                    prop.enumValueIndex = (int)pt;

                    CreatePropertyField(paramRect, pt, property);
                    paramRect.y += 22;
                }
            }
        }

        private void CreatePropertyField(Rect rect, ParamType param, SerializedProperty property)
        {
            SerializedProperty sp;
            switch (param)
            {
                case ParamType.Int:
                    sp = property.FindPropertyRelative(nameof(EventParams.Int));
                    break;
                case ParamType.String:
                    sp = property.FindPropertyRelative(nameof(EventParams.String));
                    break;
                case ParamType.TrackedObject:
                    sp = property.FindPropertyRelative(nameof(EventParams.TrackedObject));
                    break;
                default:
                    sp = property.FindPropertyRelative(nameof(EventParams.Int));
                    break;
            }

            EditorGUI.PropertyField(rect, sp);
        }

        private int GetEnumMask(SerializedProperty enumList)
        {
            if (enumList.arraySize == 0)
                return 0;

            if (enumList.arraySize == Enum.GetNames(typeof(ParamType)).Length)
                return -1;

            int mask = 0;
            var indices = new List<int>();

            for (int i = 0; i < enumList.arraySize; i++)
            {
                indices.Add(enumList.GetArrayElementAtIndex(i).enumValueIndex);
            }

            foreach (int i in indices)
            {
                mask += (int)Math.Pow(2, i);
            }

            return mask;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            int paramCount = property.FindPropertyRelative("ParamTypes").arraySize;
            return (paramCount + 2) * 22;
        }
    }
}