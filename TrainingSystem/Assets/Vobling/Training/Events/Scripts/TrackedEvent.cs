using System;
using System.Collections.Generic;
using UnityEngine;

namespace Vobling.Training.Events
{
    // TODO: Create interface for event parameters.
    [Serializable]
    public class EventParams
    {
        public List<ParamType> ParamTypes = new List<ParamType>();

        public int Int;
        public string String;
        public object Object; // for POCOs
        public TrackedObject TrackedObject;
    }

    public enum ParamType
    {
        Int,
        String,
        TrackedObject
    }

    [Serializable]
    public class TrackedEvent
    {
        public long Timestamp { get; protected set; }

        [SerializeField]
        private EventType eventType;
        public EventType EventType
        {
            get => eventType;
            set => eventType = value;
        }
        
        public EventParams EventParams;

        public TrackedEvent(long timestamp, EventType eventType, EventParams eventParams = null)
        {
            Timestamp = timestamp;
            this.eventType = eventType;
            EventParams = eventParams;
        }
    }

    public abstract class TrackedEvent<T1> : TrackedEvent
    { 
        public abstract T1 t1 { get; }

        public TrackedEvent(long timestamp, EventType eventType) : base(timestamp, eventType)
        {
        }
    }

    public abstract class TrackedEvent<T1, T2> : TrackedEvent
    {
        public abstract T1 t1 { get; }
        public abstract T1 t2 { get; }

        public TrackedEvent(long timestamp, EventType eventType) : base(timestamp, eventType)
        {
        }
    }
}
