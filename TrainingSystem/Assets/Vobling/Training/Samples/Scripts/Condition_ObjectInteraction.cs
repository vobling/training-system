﻿using Vobling.Training.Tasks;
using Vobling.Training.Events;
using EventType = Vobling.Training.Events.EventType;

namespace Vobling.Training.Samples
{
    public class Condition_ObjectInteraction : Condition
    {
        public TrackedObject TargetObject;

        public override bool CheckState()
        {
            return IsCleared;
        }

        public override bool HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            if (publishedEvent.EventType != EventType.ObjectInteraction)
                return IsCleared;

            if (publishedEvent.EventParams.TrackedObject == TargetObject)
            {
                IsCleared = true;
            }

            return IsCleared;
        }
    }
}