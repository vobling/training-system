using System.Linq;
using UnityEngine;
using UnityEditor;

using Vobling.Training.Events;

public class TrainingMain : MonoBehaviour
{
    // Set up dependencies, event pub-sub, etc.
    private void Awake()
    {
        IEventHub eventHub = new TrainingEventHub();

        foreach (var subscriber in Resources.FindObjectsOfTypeAll(typeof(MonoBehaviour)).Where(s => s is ISubscriber))
        {
            if (EditorUtility.IsPersistent(subscriber))
                continue;

            (subscriber as ISubscriber).EventHub = eventHub;
        }

        foreach (var publisher in Resources.FindObjectsOfTypeAll(typeof(MonoBehaviour)).Where(s => s is IPublisher))
        {
            if (EditorUtility.IsPersistent(publisher))
                continue;

            (publisher as IPublisher).EventHub = eventHub;
        }
    }
}

