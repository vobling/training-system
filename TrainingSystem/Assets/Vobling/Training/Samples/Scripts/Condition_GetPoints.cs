﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vobling.Training.Tasks;
using Vobling.Training.Events;

namespace Vobling.Training.Samples
{
    public class Condition_GetPoints : Condition
    {
        public int PointTarget;
        public int CurrentPoints = 0;

        public bool NeedExact = true;

        public override bool CheckState()
        {
            return IsCleared;
        }

        public override bool HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            if (publishedEvent.EventType != Events.EventType.ButtonPressed)
                return IsCleared;

            var points = publishedEvent.EventParams.Int;
            CurrentPoints += points;
            
            if (NeedExact)
            {
                IsCleared = (CurrentPoints == PointTarget);
            }
            else
            {
                IsCleared = (CurrentPoints >= PointTarget);
            }

            return IsCleared;
        }
    }
}