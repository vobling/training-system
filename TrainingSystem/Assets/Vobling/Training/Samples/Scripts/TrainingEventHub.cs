using System.Collections.Generic;

namespace Vobling.Training.Events
{
    public class TrainingEventHub : IEventHub
    {
        public Dictionary<EventType, List<ISubscriber>> SubscribersByEventType;

        public TrainingEventHub()
        {
            SubscribersByEventType = new Dictionary<EventType, List<ISubscriber>>();
        }

        public void Publish(TrackedEvent trackedEvent)
        {
            foreach (ISubscriber s in SubscribersByEventType[trackedEvent.EventType])
            {
                s.HandleTrackedEvent(trackedEvent);
            }
        }

        public void Subscribe(ISubscriber subscriber, EventType eventType)
        {
            if (!SubscribersByEventType.ContainsKey(eventType))
                SubscribersByEventType.Add(eventType, new List<ISubscriber>());

            SubscribersByEventType[eventType].Add(subscriber);
        }

        public void Subscribe(ISubscriber subscriber, IEnumerable<EventType> eventTypes)
        {
            foreach (EventType et in eventTypes)
            {
                Subscribe(subscriber, et);
            }
        }

        public void Unsubscribe(ISubscriber subscriber, EventType eventType)
        {
            SubscribersByEventType[eventType].Remove(subscriber);
        }

        public void Unsubscribe(ISubscriber subscriber, IEnumerable<EventType> eventTypes)
        {
            foreach (EventType et in eventTypes)
            {
                Unsubscribe(subscriber, et);
            }
        }
    }
}
