﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Vobling.Training.Events;

namespace Vobling.Training.Samples
{
    public class CounterText : MonoBehaviour
    {
        public int Count;
        public Text Text;

        public void CountUp()
        {
            Count++;
            Text.text = Count.ToString();
        }

        public void Add(TrackedEvent trackedEvent)
        {
            Count += trackedEvent.EventParams.Int;
            Text.text = Count.ToString();
        }
    }
}