﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vobling.Training.Tasks;
using Vobling.Training.Events;

namespace Vobling.Training.Samples
{
    public class Condition_IsObjectWithinBounds : Condition
    {
        public Transform TargetObject;
        public Collider ColliderBounds;

        public override bool CheckState()
        {
            IsCleared = ColliderBounds.bounds.Contains(TargetObject.position);
            return IsCleared;
        }

        public override bool HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            return IsCleared;
        }
    }
}