﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vobling.Training.Events;
using Vobling.Training.Tasks;

namespace Vobling.Training.Samples
{
    public class TaskCompletionUI : MonoBehaviour
    {
        public Text GetPointsTaskText;
        public Text PressAllButtonsTaskText;

        public void HandleTrackedEvent(object trackedEvent)
        {
            TrackedEvent te = trackedEvent as TrackedEvent;
            if (te.EventType == Events.EventType.TaskCompleted)
            {
                HandleTaskComplete(te.EventParams.TrackedObject.GetComponent<Task>());
            }
            else if (te.EventType == Events.EventType.TaskFailed)
            {
                HandleTaskFailure(te.EventParams.TrackedObject.GetComponent<Task>());
            }
        }

        private void HandleTaskComplete(Task completedTask)
        {
            if (completedTask.Name == "Get points")
            {
                GetPointsTaskText.color = Color.green;
            }

            if (completedTask.Name == "Press all buttons")
            {
                PressAllButtonsTaskText.color = Color.green;
            }
        }

        private void HandleTaskFailure(Task failedTask)
        {
            if (failedTask.Name == "Get points")
            {
                GetPointsTaskText.color = Color.red;
            }

            if (failedTask.Name == "Press all buttons")
            {
                PressAllButtonsTaskText.color = Color.red;
            }
        }
    }
}