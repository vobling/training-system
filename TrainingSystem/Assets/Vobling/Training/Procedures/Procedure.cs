﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Vobling.Training.Events;
using EventType = Vobling.Training.Events.EventType;

namespace Vobling.Training.Tasks
{
    public static class TimeExtensions
    { 
        public static long ToMilliseconds(this float seconds)
        {
            return (long)(seconds * 1000f);
        }
    }


    public interface IProcedure
    {
        IList<Task> Tasks { get; }
    }

    [RequireComponent(typeof(TrackedObject))]
    public abstract class Procedure : MonoBehaviour, ISubscriber, IPublisher, IProcedure
    {
        public bool Active { get; set; } = true;
        public bool IsComplete { get; protected set; }

        [SerializeField]
        [ReorderableList]
        private List<Task> tasks;
        public IList<Task> Tasks => tasks;

        public IEventHub EventHub { get; set; }

        private EventResponse eventResponse;
        public EventResponse EventResponse => eventResponse;

        protected virtual void Awake()
        {
            // Subscribe to assigned Tasks.
            eventResponse = new EventResponse();
            eventResponse.AddListener(HandleTrackedEvent);
        }

        protected virtual void Start()
        {
            EventHub.Subscribe(this, new EventType[] { EventType.TaskCompleted, EventType.TaskFailed });
        }

        protected virtual void OnDestroy()
        {
            EventHub.Unsubscribe(this, new EventType[] { EventType.TaskCompleted, EventType.TaskFailed });
        }

        protected virtual void OnProcedureCompleted()
        {
            // Publish a ProcedureComplete event.
            var completeEvent = new TrackedEvent(Time.time.ToMilliseconds(), EventType.ProcedureCompleted, new EventParams() { TrackedObject = this.GetComponent<TrackedObject>() });
            Publish(completeEvent);
        }

        protected virtual void OnProcedureFailed()
        {
            // Publish a ProcedureFailed event.
            var failEvent = new TrackedEvent(Time.time.ToMilliseconds(), EventType.ProcedureFailed, new EventParams() { TrackedObject = this.GetComponent<TrackedObject>() });
            Publish(failEvent);
        }

        public void Publish(TrackedEvent trackedEvent)
        {
            EventHub.Publish(trackedEvent);
        }

        public abstract void HandleTrackedEvent(TrackedEvent publishedEvent);
    }
}