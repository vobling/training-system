﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Vobling.Training.Events;
using EventType = Vobling.Training.Events.EventType;

namespace Vobling.Training.Tasks
{
    /// <summary>
    /// Default Procedure behaviour.
    /// Publishes ProcedureComplete when all Tasks in the collection are completed (in no particular order).
    /// Publishes ProcedureFailed when any Task in the collection is failed.
    /// Deactivates when either ProcedureComplete or ProcedureFailed are called.
    /// </summary>
    public class SimpleProcedure : Procedure
    {
        private HashSet<Task> PendingTasks;

        protected override void Awake()
        {
            base.Awake();
            PendingTasks = new HashSet<Task>();

            foreach (Task t in Tasks)
                PendingTasks.Add(t);
        }

        public override void HandleTrackedEvent(TrackedEvent publishedEvent)
        {
            if (!Active)
                return;

            if (publishedEvent.EventType == EventType.TaskFailed)
            {
                Active = false;
                OnProcedureFailed();
                return;
            }
            else if (publishedEvent.EventType == EventType.TaskCompleted)
            {
                Task t = publishedEvent.EventParams.TrackedObject.GetComponent<Task>();
                PendingTasks.Remove(t);

                if (PendingTasks.Count == 0)
                {
                    Active = false;
                    OnProcedureCompleted();
                    return;
                }
            }
        }
    }
}